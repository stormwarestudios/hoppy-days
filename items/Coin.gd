extends Node2D

enum { COIN_GOLD, COIN_SILVER, COIN_BRONZE }
var points = 3
var taken = false


func init(type):
	if type == COIN_GOLD:
		$AudioStreamPlayer2D.pitch_scale = 1.0
		$AnimatedSprite.play("spin_gold")
		points = 3
	elif type == COIN_SILVER:
		$AudioStreamPlayer2D.pitch_scale = 0.8
		$AnimatedSprite.play("spin_silver")
		points = 2
	elif type == COIN_BRONZE:
		$AudioStreamPlayer2D.pitch_scale = 0.6
		$AnimatedSprite.play("spin_bronze")
		points = 1
	else:
		# No overrides
		pass
	return


func _on_Area2D_body_entered(body):
	if not taken:
		taken = true
		$AnimationPlayer.play("Die")
		$AudioStreamPlayer2D.play()
		get_tree().call_group("GameState", "coin_up")
	return


func die():
	queue_free()
