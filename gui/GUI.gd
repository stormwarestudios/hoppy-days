extends CanvasLayer


func _ready():
	$Control/TextureRect/HBoxContainer/LifeCount.text = "3"


func update_lives(life_count):
	$Control/TextureRect/HBoxContainer/LifeCount.text = str(life_count)


func update_coins(coin_count):
	$Control/TextureRect/HBoxContainer/CoinCount.text = str(coin_count)
