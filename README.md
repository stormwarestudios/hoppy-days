# hoppy-days

## Playing the Game

The game may be played at [http://stormwarestudios.gitlab.io/hoppy-days/](http://stormwarestudios.gitlab.io/hoppy-days/).

## Secrets

Secret key generation may be done as a one-liner using the following command:
```
node -e "require('crypto').randomBytes(64, function(ex, buf) { console.log(buf.toString('hex')) });"
```

