extends KinematicBody2D

var motion = Vector2(0, 0)

const SPEED = 1000
const GRAVITY = 250
const UP = Vector2(0, -1)
const JUMP_SPEED = 4000
const WORLD_LIMIT = 6000

export var boost_multiplier = 2.0
export var jump_offset = 5.0

signal animate


func _physics_process(delta):
	apply_gravity()
	jump()
	move()
	animate()
	move_and_slide(motion, UP)


func move():
	if Input.is_action_pressed("player_left") and not Input.is_action_pressed("player_right"):
		motion.x = -SPEED
	elif Input.is_action_pressed("player_right") and not Input.is_action_pressed("player_left"):
		motion.x = SPEED
	else:
		motion.x = 0


func animate():
	emit_signal("animate", motion)


func jump():
	if Input.is_action_pressed("player_jump") and is_on_floor():
		motion.y -= JUMP_SPEED
		SFX.play("jump1")


func apply_gravity():
	if position.y > WORLD_LIMIT:
		get_tree().call_group("GameState", "end_game")
	if is_on_floor() and motion.y > 0:
		motion.y = 0
	elif is_on_ceiling():
		motion.y = 1
	else:
		motion.y += GRAVITY


func hurt():
	SFX.play("pain")
	position.y -= jump_offset
	yield(get_tree(), "idle_frame")
	motion.y = -JUMP_SPEED


func boost():
	SFX.play("jump1")
	position.y -= jump_offset
	yield(get_tree(), "idle_frame")
	motion.y -= boost_multiplier * JUMP_SPEED
