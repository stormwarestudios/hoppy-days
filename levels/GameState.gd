extends Node2D

export var max_lives = 5
var lives = max_lives
var coins_per_life = 10
var coins = 0


func coin_up():
	coins += 1
	get_tree().call_group("GUI", "update_coins", coins)
	var multiple_of_coins = (coins % coins_per_life) == 0
	if multiple_of_coins:
		life_up()


func life_up():
	lives += 1
	update_GUI()


func update_GUI():
	get_tree().call_group("GUI", "update_lives", lives)


func _ready():
	update_GUI()
	SFX.play("BGM")
	lives = max_lives
	add_to_group("GameState")


func _on_Level1_tree_exiting():
	SFX.stop_all()


func hurt():
	lives -= 1
	$Player.hurt()
	if lives < 0:
		end_game()
	update_GUI()


func end_game():
	get_tree().change_scene("res://levels/GameOver.tscn")


func win_game():
	get_tree().change_scene("res://levels/Victory.tscn")
