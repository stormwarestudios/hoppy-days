extends Node

func _ready():
	print(name + " ready")

func play(sfx = null):
	if sfx and get_node(sfx):
		get_node(sfx).play()
	else:
		print("Unable to play sfx " + sfx)

func stop(sfx):
	if sfx:
		get_node(sfx).stop()
	else:
		print("Unable to stop sfx " + sfx)

func stop_all():
	for sfx in get_children():
		sfx.stop()

